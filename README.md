# Spring 2024 IDS721 Cloud Computing Zola Static Website

## Website

Please see the website here [Personal Zola Website](https://zola-website-shanghuiyin-0954779285f5bfaf245e09ddfb11f7674ba432.gitlab.io/).


> Website Screenshot:

![Alt text](image.png)

## Requirement

1. Website built with ZolaLinks to an external site., Hugo, Gatsby, Next.js or equivalent
2. GitLab workflow to build and deploy site on push
3. Hosted on Vercel, Netlify, AWS Amplify, AWS S3, or others.


## Prerequisites

In order to realise this website, you need some software pre-installed:

- [Git](https://git-scm.com/downloads), Required for version control.

- [Node](https://nodejs.org/en/download), an open-source, cross-platform JavaScript runtime environment.

- [Zola](https://github.com/getzola/zola/releases), a fast static site generator.

- an editor or integrated development environment of your choice - I use [JetBrains IDEA](https://www.jetbrains.com/idea/download),
  an IDE that makes development a more productive and enjoyable experience. 


## Zola File Structure

This is the directory structure of the stand-alone site, where the theme is in the root directory:

```
/
├── css
├── i18n
├── static
│   ├── css
│   ├── img
│   └── js
├── templates
└── theme.toml
```

## License

This theme is under the MIT License.


## Reference

1. https://www.getzola.org/
2. https://www.getzola.org/themes/
3. https://www.getzola.org/documentation/getting-started/overview/
