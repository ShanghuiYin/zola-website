+++
date = 2024-01-25
title = "Portfolio Project Page Templates"
description = "This is a template page for IDS721 Projects."
authors = ["Shanghui Yin"]
[taxonomies]
tags = ["Zola"]
[extra]
math = false
image = "banner.jpg"
+++


# Project Page Template


## Overview

THIS is IDS721 Week 1 Mini-Project: portfolio project page templates.


## Technologies Used

- Technology 1
- Technology 2

## Results

TBD
Explain the results of your project.


## Reference

1. https://www.getzola.org/
2. https://www.getzola.org/themes/


---
---


Return to [Home](/)
